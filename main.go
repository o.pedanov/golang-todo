package main

import (
	"database/sql"
	"golang-todo/controllers"
	"golang-todo/driver"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

var db *sql.DB

func main() {
	db = driver.ConnectDB()

	r := mux.NewRouter()

	controller := controllers.Controller{}

	r.HandleFunc("/tasks", controller.GetTasks(db)).Methods("GET")
	r.HandleFunc("/tasks/{id}", controller.GetTask(db)).Methods("GET")
	r.HandleFunc("/tasks", controller.AddTask(db)).Methods("POST")
	r.HandleFunc("/tasks/{id}", controller.UpdateTask(db)).Methods("PUT")
	r.HandleFunc("/tasks/{id}", controller.RemoveTask(db)).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8000", r))
}
