package tasksRepository

import (
	"database/sql"
	"golang-todo/models"
	"log"
)

type TasksRepository struct{}

func logError(err error) {
	if err != nil {
		log.Println(err)
	}
}

func (t TasksRepository) GetTasks(db *sql.DB, task models.Task, tasks []models.Task) []models.Task {
	rows, err := db.Query("select * from tasks")
	logError(err)

	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(&task.ID, &task.Name, &task.Description, &task.DueDate, &task.Status)
		logError(err)

		tasks = append(tasks, task)
	}
	return tasks
}

func (t TasksRepository) GetTask(db *sql.DB, task models.Task, id int) models.Task {
	rows := db.QueryRow("select * from tasks where id=$1", id)
	err := rows.Scan(&task.ID, &task.Name, &task.Description, &task.DueDate, &task.Status)
	logError(err)

	return task
}

func (t TasksRepository) AddTask(db *sql.DB, task models.Task) int {
	var id int
	err := db.QueryRow("insert into tasks (name, description, duedate, status) values($1, $2, $3, $4) RETURNING id;",
		task.Name, task.Description, task.DueDate, task.Status).Scan(id)
	logError(err)
	return id
}

func (t TasksRepository) UpdateTask(db *sql.DB, task models.Task, id int) int64 {
	result, err := db.Exec("update tasks set name=$2, description=$3, duedate=$4, status=$5 where id=$1 RETURNING id",
		&id, &task.Name, &task.Description, &task.DueDate, &task.Status)
	logError(err)

	rowsUpdated, err := result.RowsAffected()
	logError(err)

	return rowsUpdated
}

func (t TasksRepository) RemoveTask(db *sql.DB, id int) int64 {
	result, err := db.Exec("delete from tasks where id = $1", id)
	logError(err)

	rowsDeleted, err := result.RowsAffected()
	logError(err)
	return rowsDeleted
}
