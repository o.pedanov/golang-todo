package controllers

import (
	"database/sql"
	"encoding/json"
	"golang-todo/models"
	tasksRepository "golang-todo/repository/tasks"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type Controller struct{}

var tasks []models.Task
var statuses = map[string]bool{
	"новый":       false,
	"выполняется": false,
	"отменен":     false,
	"выполнен":    false,
	"просрочен":   false,
}

func (c Controller) GetTasks(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var task models.Task
		tasks = []models.Task{}
		tasksRepo := tasksRepository.TasksRepository{}

		tasks = tasksRepo.GetTasks(db, task, tasks)

		json.NewEncoder(w).Encode(tasks)

	}
}

func (c Controller) GetTask(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var task models.Task
		params := mux.Vars(r)
		id, err := strconv.Atoi(params["id"])
		if err != nil {
			log.Println("Can't find task: ID expected, received '", params["id"], "'")
			json.NewEncoder(w).Encode(0)
		}

		tasks = []models.Task{}
		tasksRepo := tasksRepository.TasksRepository{}

		task = tasksRepo.GetTask(db, task, id)

		json.NewEncoder(w).Encode(task)

	}
}

func (c Controller) AddTask(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var (
			task   models.Task
			taskID int
		)

		json.NewDecoder(r.Body).Decode(&task)

		if _, ok := statuses[task.Status]; !ok {
			log.Println("Wrong status:'", task.Status, "'. Task is not added.")
			json.NewEncoder(w).Encode(0)
			return
		}

		taskRepo := tasksRepository.TasksRepository{}
		taskID = taskRepo.AddTask(db, task)

		json.NewEncoder(w).Encode(taskID)
	}
}

func (c Controller) UpdateTask(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var task models.Task
		params := mux.Vars(r)
		id, err := strconv.Atoi(params["id"])
		if err != nil {
			log.Println("Can't update task: ID expected, received '", params["id"], "'")
			json.NewEncoder(w).Encode(0)
		}
		json.NewDecoder(r.Body).Decode(&task)

		if _, ok := statuses[task.Status]; !ok {
			log.Println("Wrong status:'", task.Status, "'. Task is not updated.")
			json.NewEncoder(w).Encode(0)
			return
		}

		taskRepo := tasksRepository.TasksRepository{}
		rowsUpdated := taskRepo.UpdateTask(db, task, id)

		json.NewEncoder(w).Encode(rowsUpdated)
	}
}

func (c Controller) RemoveTask(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		id, err := strconv.Atoi(params["id"])
		if err != nil {
			log.Println("Can't remove task: ID expected, received '", params["id"], "'")
			json.NewEncoder(w).Encode(0)
		}

		taskRepo := tasksRepository.TasksRepository{}
		rowsDeleted := taskRepo.RemoveTask(db, id)

		json.NewEncoder(w).Encode(rowsDeleted)

	}
}
