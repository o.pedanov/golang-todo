# Golang ToDo

Simple ToDo REST app with PSQL DB

To run the database docker-composer should be executed:
```
 docker-compose up -d
```

Database should be created:
```
docker-compose exec pgdb psql -U db_user -c 'CREATE DATABASE db_tasks'
```

Table should be created:
```
docker-compose exec pgdb psql db_tasks -U db_user -c 'CREATE TABLE tasks (id SERIAL NOT NULL, name VARCHAR(100), description VARCHAR(100), duedate TIMESTAMP, status VARCHAR(100));'
```

Sample templates for adding tasks:
```
{"name":"First task","description":"First task description","duedate":"2021-04-20T21:10:00+03:00","status":"error"}
{"name":"Second task","description":"Bla-bla 2","duedate":"2021-05-20T21:00:00+03:00","status":"status"}
{"name":"Third task","description":"Bla-bla 3","duedate":"2021-06-20T21:00:41+03:00","status":"will not be added"}
{"name":"Forth task","description":"Bla-bla 4","duedate":"2021-07-20T21:00:41+03:00","status":"новый"}
{"name":"Fifth task","description":"Bla-bla 5","duedate":"2021-08-20T21:00:41+03:00","status":"выполняется"}
{"name":"Sixs task","description":"Bla-bla 6","duedate":"2021-09-20T21:00:41+03:00","status":"отменен"}
{"name":"Sevens task","description":"Bla-bla 7","duedate":"2021-10-20T21:00:41+03:00","status":"выполнен"}
{"name":"Eights task","description":"Bla-bla  8","duedate":"2021-11-20T21:00:00+03:00","status":"просрочен"}
```
